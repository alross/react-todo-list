import React, { Component } from 'react'

export class Header extends Component {
    render() {
        return (
            <div style={styling}>
                <h1>To Do</h1>
            </div>
        )
    }
}

const styling = {
    background: '#333333',
    color: '#ffffff',
    textAlign: 'center',
    padding: '10px'
}

export default Header
