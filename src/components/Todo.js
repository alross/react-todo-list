import React, { Component } from 'react'

export class Todo extends Component {

    constructor(props) {
        super(props);
        this.toggleComplete = this.toggleComplete.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        
        this.state = {
            id: this.props.id,
            title: this.props.title,
            completed: this.props.completed
        }
    }

    containerStyle() { return {
        padding: '1em',
        width: '100%',
        height: '3em',
        background: this.state.completed ? '#8c8c8c' : '#CEFAFF',
        border: '1px #cccccc dotted'
        };
    }

    titleStyle() { return {
        float: 'left',
        textDecoration: this.state.completed ? 'line-through' : 'none'
        };
    }

    toggleStyle() { return this.state.completed ? this.completeStyle() : this.incompleteStyle(); }

    incompleteStyle() { return {
        width: '2em',
        height: '2em',
        background: '#ffffff',
        color: '#808080',
        border: 'none',
        borderRadius: '50%',
        cursor: 'pointer',
        fontWeight: 'bold',
        padding: '0 auto 0 auto',
        float: 'right',
        marginLeft: '0.5em',
        marginRight: '0.5em'
        };
    }

    completeStyle() { return {
        width: '2em',
        height: '2em',
        background: '#000000',
        color: '#808080',
        border: 'none',
        borderRadius: '50%',
        cursor: 'pointer',
        fontWeight: 'bold',
        padding: '0 auto 0 auto',
        float: 'right',
        marginLeft: '0.5em',
        marginRight: '0.5em'
        };
    }

    deleteStyle() { return{
        width: '2em',
        height: '2em',
        background: '#930000',
        color: '#ffffff',
        border: 'none',
        borderRadius: '50%',
        cursor: 'pointer',
        fontWeight: 'bold',
        padding: '0 auto 0 auto',
        float: 'right',
        marginLeft: '0.5em',
        marginRight: '0.5em'
        }
    }

    toggleComplete() {
        this.setState({completed: !this.state.completed});
    }

    handleDelete() {
        this.props.deleteTodo(this.state.id);
    }

    render() {
        return (
            <div style={this.containerStyle()}>
                <span style={this.titleStyle()}>{this.state.title}</span>
                <button style={this.toggleStyle()} onClick={this.toggleComplete}></button>
                <button style={this.deleteStyle()} onClick={this.handleDelete}>X</button>
            </div>
        )
    }
}

export default Todo
