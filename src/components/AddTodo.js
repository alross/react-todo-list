import React, { Component } from 'react'

export class AddTodo extends Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

        this.state = {
            newTodo: {
                id: 0,
                title: '',
                completed: false
            }
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.addTodo(this.state.newTodo);
        this.setState({newTodo: { title: '' }});
    }

    onChange(e) {
        this.setState({ 
        newTodo: {id: this.props.todos.length, title: e.target.value, completed: false},
        [e.target.name]: e.target.value })
    };

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit} style={{display: 'flex'}}>
                    <input type="text" name="title" placeholder="Add Entry..." value={this.state.newTodo.title} onChange={this.onChange} style={textStyle} />
                    <input type="submit" value="Submit" className="btn" style={submitStyle} />
                </form>
            </div>
        )
    }
}

const textStyle = {
    flex: '5',
    padding: '0.5em',
    fontSize: '16px'
}

const submitStyle = {
    flex: '1',
    marginTop: '1em'
}

export default AddTodo
