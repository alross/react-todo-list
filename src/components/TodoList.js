import React, { Component } from 'react'
import Todo from './Todo'

export class TodoList extends Component {
    
    constructor(props) {
        super(props);
    }

    render() {
        const items = this.props.todos.map(item => <Todo key={item.id} id={item.id} title={item.title} completed={item.completed} deleteTodo={this.props.deleteTodo} />);
        return (
            <div style={containerStyle}>
                <ul>
                    {items}
                </ul>
            </div>
        )
    }
}

const containerStyle = {
    width: '90%',
    padding: '1em 0 1em 0'
}

export default TodoList
