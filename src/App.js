import React, { Component } from 'react'
import './App.css';
import Header from './components/layout/Header'
import Todo from './components/Todo'
import AddTodo from './components/AddTodo'
import TodoList from './components/TodoList'

export default class App extends Component {

  constructor(props) {
    super(props);

    this.addTodo = this.addTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);

    this.state = {
      todos: [
        {
          id: 0,
          title: 'Shop for groceries',
          completed: true
        },
        {
          id: 1,
          title: 'Mow the lawn',
          completed: false
        },
        {
          id: 2,
          title: 'Take out the trash',
          description: '',
          completed: false
        }
      ]
    }
  }

  addTodo(todo) {
    this.setState({ todos: [...this.state.todos, todo] });
    console.log(todo);
  }

  deleteTodo(id) {
    // const newTodos = this.state.todos.filter((item) => { item.id == id });
    console.log(id);
    this.setState({ todos: this.state.todos.filter(item => item.id !== id) });
  }

  render() {
    return (
      <div className="App">
        <Header />
        <AddTodo addTodo={this.addTodo} todos={this.state.todos} />
        <TodoList todos={this.state.todos} deleteTodo={this.deleteTodo} />
      </div>
    );
  }
}